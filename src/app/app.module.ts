import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DemoStyleComponent } from './modules/demo-style/demo-style.component';
import { PublisherComponent } from './modules/publisher/publisher.component';
import { BookComponent } from './modules/book/book.component';
import { AuthorComponent } from './modules/author/author.component';
import { CategoryComponent } from './modules/category/category.component';
import { ListComponent } from './modules/author/list/list.component';
import { AddComponent } from './modules/author/add/add.component';
import { UpdateComponent } from './modules/author/update/update.component';

@NgModule({
  declarations: [
    AppComponent,
    DemoStyleComponent,
    PublisherComponent,
    BookComponent,
    AuthorComponent,
    CategoryComponent,
    ListComponent,
    AddComponent,
    UpdateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
